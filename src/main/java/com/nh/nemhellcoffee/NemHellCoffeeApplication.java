package com.nh.nemhellcoffee;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NemHellCoffeeApplication {

	public static void main(String[] args) {
		SpringApplication.run(NemHellCoffeeApplication.class, args);
	}

}
