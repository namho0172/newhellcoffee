package com.nh.nemhellcoffee.controller;

import com.nh.nemhellcoffee.entity.Member;
import com.nh.nemhellcoffee.model.hell.HellRequest;
import com.nh.nemhellcoffee.model.hell.HellStaticsResponse;
import com.nh.nemhellcoffee.service.HellService;
import com.nh.nemhellcoffee.service.MemberService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/hell")
public class HellController {
    private final MemberService memberService;
    private final HellService hellService;

    @PostMapping("/new/member/{memberId}")
    public String setHell(@PathVariable long memberId, @RequestBody HellRequest request) {
        Member member = memberService.getData(memberId);
        hellService.setHell(member, request);

        return "OK";
    }
    @GetMapping("/statics")
    public HellStaticsResponse getStatics(){
        return hellService.getStatics();
    }
}
