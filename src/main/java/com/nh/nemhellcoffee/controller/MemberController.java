package com.nh.nemhellcoffee.controller;

import com.nh.nemhellcoffee.entity.Member;
import com.nh.nemhellcoffee.model.member.MemberChangeRequest;
import com.nh.nemhellcoffee.model.member.MemberRequest;
import com.nh.nemhellcoffee.service.HellService;
import com.nh.nemhellcoffee.service.MemberService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member")
public class MemberController {
    private final MemberService memberService;
    private final HellService hellService;

    @PostMapping("/sign-up")
    public String setMember(@RequestBody MemberRequest request){
        memberService.setMember(request);

        return "OK";
    }
    @GetMapping("/all")
    public List<Member> getMembers(){
       return memberService.getMembers();
    }
    @PutMapping("/{id}")
    public String putMember(@PathVariable long id,@RequestBody MemberChangeRequest request){
        memberService.putMember(id, request);

        return "OK";
    }
}
