package com.nh.nemhellcoffee.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class Hell {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "memberId", nullable = false)
    private Member member;

    @Column(nullable = false)
    private Double totalCost;

    @Column(nullable = false)
    private LocalDate dateCost;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "memberTotal", nullable = false)
    private Member memberTotal;
}
