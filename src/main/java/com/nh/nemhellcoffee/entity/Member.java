package com.nh.nemhellcoffee.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.Stack;

@Entity
@Getter
@Setter
public class Member {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    private Long id;

    @Column(nullable = false, length = 30)
    private String name;

    @Column(nullable = false, length = 13, unique = true)
    private String phoneNumber;

    @Column(nullable = false)
    private Double spending;

    @Column(nullable = false)
    private Long total;
}
