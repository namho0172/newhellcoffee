package com.nh.nemhellcoffee.model.hell;

import com.nh.nemhellcoffee.entity.Member;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;


@Getter
@Setter
public class HellRequest {
    private Member member;
    private Double totalCost;
}
