package com.nh.nemhellcoffee.model.member;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberChangeRequest {
    private String spending;
}
