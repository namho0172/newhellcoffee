package com.nh.nemhellcoffee.model.member;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberRequest {
    private String name;
    private String phoneNumber;
    private Double spending;
    private Long total;
}
