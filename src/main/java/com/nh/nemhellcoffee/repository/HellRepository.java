package com.nh.nemhellcoffee.repository;
import com.nh.nemhellcoffee.entity.Hell;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HellRepository extends JpaRepository<Hell, Long> {
}
