package com.nh.nemhellcoffee.repository;

import com.nh.nemhellcoffee.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MemberRepository extends JpaRepository<Member, Long> {
}
