package com.nh.nemhellcoffee.service;

import com.nh.nemhellcoffee.entity.Hell;
import com.nh.nemhellcoffee.entity.Member;
import com.nh.nemhellcoffee.model.hell.HellRequest;
import com.nh.nemhellcoffee.model.hell.HellStaticsResponse;
import com.nh.nemhellcoffee.repository.HellRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
@RequiredArgsConstructor
public class HellService {
    private final HellRepository hellRepository;

    public void setHell(Member member, HellRequest request){
        Hell addData = new Hell();
        addData.setMember(member);
        addData.setTotalCost(request.getTotalCost());
        addData.setDateCost(LocalDate.now());

        hellRepository.save(addData);
    }
   public HellStaticsResponse getStatics() {
        HellStaticsResponse response = new HellStaticsResponse();

       List<Hell> originList = hellRepository.findAll();

       long totalPick = 0;
       for (Hell hell : originList) {
           totalPick ++;
       }
       response.setMemberTotal(totalPick);
       return response;
   }


}
