package com.nh.nemhellcoffee.service;

import com.nh.nemhellcoffee.entity.Member;
import com.nh.nemhellcoffee.model.member.MemberChangeRequest;
import com.nh.nemhellcoffee.model.member.MemberRequest;
import com.nh.nemhellcoffee.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MemberService {
    private final MemberRepository memberRepository;

    public Member getData(long id){
        return memberRepository.findById(id).orElseThrow();
    }

    public void setMember(MemberRequest request){
        Member addData = new Member();
        addData.setName(request.getName());
        addData.setPhoneNumber(request.getPhoneNumber());
        addData.setSpending(request.getSpending());
        addData.setTotal(request.getTotal());

        memberRepository.save(addData);
    }
    public List<Member> getMembers(){
        List<Member> originData = memberRepository.findAll();
        List<Member> result = new LinkedList<>();

        for (Member member : originData) {
            Member addItem = new Member();
            addItem.setId(member.getId());
            addItem.setName(member.getName());
            addItem.setPhoneNumber(member.getPhoneNumber());
            addItem.setSpending(member.getSpending());
            addItem.setTotal(member.getTotal());

            result.add(addItem);
        }
        return result;
    }
    public void putMember(long id, MemberChangeRequest request){
        Member originData = memberRepository.findById(id).orElseThrow();
        originData.setSpending(Double.valueOf(request.getSpending()));

        memberRepository.save(originData);
    }
}
